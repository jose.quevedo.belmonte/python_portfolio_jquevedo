# https://www.hackerrank.com/challenges/find-substring/problem
import re
if __name__ == '__main__':
    N = int(input())
    line1 = []

    for _ in range(N):
        line1.append(input())

    M = int(input())
    for _ in range(M):
        res = 0
        regex = '(\\w+'+input()+'\\w+)'
        for x in line1:
            res += len(re.findall(regex, x))
        print(res)


