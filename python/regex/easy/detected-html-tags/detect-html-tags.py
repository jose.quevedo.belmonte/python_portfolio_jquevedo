# https://www.hackerrank.com/challenges/detect-html-tags/problem #

import re


if __name__ == '__main__':
    N = int(input())
    res = []
    flat_res = []
    regex = '<(\\w+)'
    for _ in range(N):
        res.append(re.findall(regex, input()))
    for _ in res:
        for _ in _:
            flat_res.append(_)
    print(';'.join(sorted(set(flat_res))))


