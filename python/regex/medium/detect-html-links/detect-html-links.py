# https://www.hackerrank.com/challenges/detect-html-links/problem #
import re
if __name__ == '__main__':
    N = int(input())
    regex = '<a href="(.+?)".*?> ?(?!<b>)(.*?) ?<'
    res = []
    for _ in range(N):
        line = input()

        res.append(re.findall(regex, line))

    for x in res:
        for i in x:
            print(i[0] + "," + i[1])

