# Ingest csv file
import csv

if __name__ == '__main__':

    file = '../test.csv' #Your csv file
    with open(file, "rt", encoding='iso-8859-15') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row[0])
    f.close()
