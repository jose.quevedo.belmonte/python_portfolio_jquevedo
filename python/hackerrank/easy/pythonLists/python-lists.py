#https://www.hackerrank.com/challenges/python-lists/problem
if __name__ == '__main__':
    sol = []
    N = int(input())
    while N > 0:
        line = input()
        splitline = line.split(" ")

        if splitline[0] == "insert":
            #if int(splitline[1]) >= len(sol):
            sol.insert(int(splitline[1]), int(splitline[2]))
            #else:
            #   sol[int(splitline[1])] = int(splitline[2])
        elif splitline[0] == "print":
            print(sol)
        elif splitline[0] == "remove":
            sol.remove(int(splitline[1]))
        elif splitline[0] == "append":
            sol.append(int(splitline[1]))
        elif splitline[0] == "sort":
            sol.sort()
        elif splitline[0] == "pop":
            sol.pop()
        elif splitline[0] == "reverse":
            sol.reverse()
        else:
            print("Nothing")
        N = N-1