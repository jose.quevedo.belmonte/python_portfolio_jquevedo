# https://www.hackerrank.com/challenges/string-validators/problem

import re


if __name__ == '__main__':
    cad = input()
    print(bool(re.findall("[\\w]", cad)))
    print(bool(re.findall("[A-Za-z]", cad)))
    print(bool(re.findall("[\\d]", cad)))
    print(bool(re.findall("[a-z]", cad)))
    print(bool(re.findall("[A-Z]", cad)))


