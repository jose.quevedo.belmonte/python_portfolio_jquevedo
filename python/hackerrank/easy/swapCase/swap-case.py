import re

def swap_case(s):
    res = ""
    up = re.compile("[A-Z]")
    low = re.compile("[a-z]")
    for x in list(s):
        if up.match(x):
            x = x.lower()
            res = res + x
        elif low.match(x):
            x = x.upper()
            res = res + x
        else:
            res = res + x
    return res


if __name__ == '__main__':
    line = input()
    print(swap_case(line))
