# https://www.hackerrank.com/challenges/python-mutations/problem


def mutate_string(cad, position, character):
    return cad[0:position] + character + cad[position+1:]


if __name__ == '__main__':
    line1 = input()
    line2 = input().split(" ")
    pos = int(line2[0])
    char = line2[1]
    print(mutate_string(line1, pos, char))
