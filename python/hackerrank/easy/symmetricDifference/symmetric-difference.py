#https://www.hackerrank.com/challenges/py-set-symmetric-difference-operation
if __name__ == '__main__':
    N = int(input())
    cad1 = set(input().split(" "))
    M = int(input())
    cad2 = set(input().split(" "))
    print(len(cad1.symmetric_difference(cad2)))
