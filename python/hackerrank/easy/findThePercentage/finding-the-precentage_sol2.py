#https://www.hackerrank.com/challenges/finding-the-percentage/problem
if __name__ == '__main__':
    N = int(input())
    student_marks = {}
    for _ in range(N):
        name, *line = input().split()
        califications = list(map(float, line))
        student_marks[name] = califications
    query = input()
    print("%.2f" % (sum(student_marks[query]) / len(student_marks[query])))









