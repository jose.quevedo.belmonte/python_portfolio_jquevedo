#https://www.hackerrank.com/challenges/finding-the-percentage/problem
def func(sol, query):
    res = float(0)
    for student in sol:
        exist = False
        sum = float(0)
        acc = 0
        for x in student:
            if x == query:
                exist = True
            elif exist is True:
                sum = sum + x
                acc = acc + 1
        res = sum / acc
    return res


if __name__ == '__main__':
    sol = []
    N = int(input())
    while N > 0:
        line = input()
        student = []
        for word in line.split(" "):
            if word == line.split(" ")[0]:
                student.append(word)
            else:
                student.append(float(word))
        sol.append(student)
        N = N-1
    query = input()
    print(func(sol, query))









