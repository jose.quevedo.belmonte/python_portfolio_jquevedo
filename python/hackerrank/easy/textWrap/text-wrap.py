# https://www.hackerrank.com/challenges/text-wrap/problem
import textwrap


def wrap(string, max_width):
    textwrap.wrap(string, max_width)
    return textwrap.wrap(string, max_width)


def wrap2(string, max_width):
    return "\n".join([string[i:i + max_width] for i in range(0, len(string), max_width)])


if __name__ == '__main__':
    cad1 = input()
    cad2 = int(input())
    #wrap2(cad1, cad2)
    for i in wrap(cad1, cad2):
       print(i)

