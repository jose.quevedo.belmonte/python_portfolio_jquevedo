# https://www.hackerrank.com/challenges/python-string-split-and-join


def split_and_join(line):
    return line.replace(" ", "-")


if __name__ == '__main__':
    cad = input()
    print(split_and_join(cad))
