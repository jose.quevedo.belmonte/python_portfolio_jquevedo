# https://www.hackerrank.com/challenges/nested-list/probl #
import numpy as np
if __name__ == '__main__':
    name = ""
    #sol = np.ndarray
    sol = []
    N = int(input()) * 2
    while N > 0:
        line = input()
        if N % 2 is 0:
            name = line
        else:
            sol.append([name, int(line)])
            name = ""
        N = N-1

    #sol.sort(reverse=True)
    sol.sort(key=lambda x: x[1:])
    print(sol)
