from statistics import median
if __name__ == '__main__':
    n = int(input())
    arr = [int(x) for x in input().split()]
    rep = [int(x) for x in input().split()]
    res = []
    for x in range(n):
        res += [arr[x]] * rep[x]
    res.sort()
    print(res)
    t = int(len(res) / 2)
    if len(res) % 2 == 0:
        L = res[:t]
        U = res[t:]
    else:
        L = res[:t]
        U = res[t + 1:]

    print('%.1f' % (float(median(U))-float(median(L))))
