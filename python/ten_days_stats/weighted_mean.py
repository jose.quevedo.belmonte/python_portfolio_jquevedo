# Weighted Mean
if __name__ == '__main__':
    N = 0
    number = int(input())
    x = list(map(int, input().split(" ")))
    w = list(map(int, input().split(" ")))
    res = []

    while N != len(x):
        res.append(x[N]*w[N])
        N = N+1

    print("%.1f" % (sum(res)/sum(w)))

