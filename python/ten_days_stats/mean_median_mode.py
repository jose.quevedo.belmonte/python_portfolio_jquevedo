import numpy as np
from scipy import stats

if __name__ == '__main__':

    number = int(input())
    values = list(map(int, input().split(" ")))
    print(np.mean(values))
    print(np.median(values))
    print(int(stats.mode(values)[0]))


